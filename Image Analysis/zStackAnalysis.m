%% Settings %%
zStacksSettings.Width = 33.2106; % microns
zStacksSettings.Height = 33.2106; % microns
zStacksSettings.Depth = 14.2; % microns
zStacksSettings.Dimensions = [1000 1000 42]; %[x y z]
zStacksSettings.Voxelsize = [0 0 0];
zStacksSettings.Voxelsize(1) = zStacksSettings.Width  /  zStacksSettings.Dimensions(1);
zStacksSettings.Voxelsize(2) = zStacksSettings.Height  /  zStacksSettings.Dimensions(2);
zStacksSettings.Voxelsize(3) = zStacksSettings.Depth  /  zStacksSettings.Dimensions(3);
zStacksSettings.voxelVolume = prod(zStacksSettings.Voxelsize); %um³
bgMultiplier = 1.01;
load("plotSettings.mat");
%% Import Image Data and Labels %% 

folder = "D:\OneDrive\WSI-Doktorarbeit\Matlab\Matlab_for_everybody\Image Analysis\Images\Reihe\pA_cpmTq2-GR + Cycloheximid + 100nm Dexa_30min_02";
fileNameNuclearReceptor = "pA_cpmTq2-GR + Cycloheximid + 100nm Dexa_30min_02.tif";


%Ohne Histogramme
[zStacks, labels, volume, intensity, intensityDividedByVolume] = ...
    zStackAnalysisFunction(bgMultiplier, zStacksSettings, folder, fileNameNuclearReceptor);

%Mit Histogramme
%[zStacks, labels, volume, intensity, intensityDividedByVolume, histoCell, histoCytoplasmaCalc, histoCytoplasmaDrawn, histoNucleus] = ...
%    zStackAnalysisFunction(bgMultiplier, zStacksSettings, folder, fileNameNuclearReceptor);

%[zStacks, labels, volume, intensity, intensityDividedByVolume, histoCell, histoCytoplasmaCalc, histoCytoplasmaDrawn, histoNucleus] = ...
%    clusterAnalysisFunction(bgMultiplier, zStacksSettings, folder, fileNameNuclearReceptor, fileNameNucleus, fileNameCytoplasma);

%VolumePlot(zStacksSettings, zStacks.NuclearReceptor.IntensitiesWithoutBackground, plotSettings, "grad");


