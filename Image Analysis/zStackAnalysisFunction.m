function [zStacks, labels, volume, intensity, intensityDividedByVolume, histoCell, histoCytoplasmaCalc, histoCytoplasmaDrawn, histoNucleus]  = zStackAnalysisFunction(bgMultiplier, zStacksSettings,folder ,fileNameNuclearReceptor, fileNameNucleus, fileNameCytoplasma)

    % Check for required arguments
    if nargin < 3
        error('clusterAnalysisFunction requires at least bgMultiplier and zStacksSettings and folder.');
    end

    % If fileNameNuclearReceptor is not provided, use fileNameNucleus for both
    if nargin < 6
        fileNameNucleus = fileNameNuclearReceptor;
        fileNameCytoplasma = fileNameNuclearReceptor;
    end

    % Check for valid folder and file paths
if ~(isstring(folder)  || ~ischar(folder)) || ...
   ~(isstring(fileNameNuclearReceptor) || ~ischar(fileNameNuclearReceptor)) || ...
   ~(isstring(fileNameNucleus) || ~ischar(fileNameNucleus)) || ...
   ~(isstring(fileNameCytoplasma) || ~ischar(fileNameCytoplasma))
    error('Invalid input: folder and file names must be strings or character arrays.');
end

disp("Reading in data")
filePathStainingNucleus = fullfile(folder, fileNameNucleus);
filePathStainingCytoplasma = fullfile(folder, fileNameCytoplasma);
filePathNuclearReceptor = fullfile(folder, fileNameNuclearReceptor);


zStacks.Nucleus.Intensities = tiffreadVolume(filePathStainingNucleus);
zStacks.Cytoplasma.Intensities = tiffreadVolume(filePathStainingCytoplasma);
zStacks.NuclearReceptor.Intensities = tiffreadVolume(filePathNuclearReceptor);

labels.Nucleus.Drawn = load(fullfile(folder,'labelNucleus.mat')).labels;
labels.Cell.Drawn = load(fullfile(folder,'labelCytoplasma.mat')).labels;

labels.BackgroundCytoplasma = load(fullfile(folder,'labelCytoplasmaBG.mat')).labels;
disp("All data succesfully loaded")
%% Calculate Backgrounds
zStacks.Nucleus.Background = median(zStacks.Nucleus.Intensities(labels.BackgroundCytoplasma));
zStacks.Cytoplasma.Background = median(zStacks.Cytoplasma.Intensities(labels.BackgroundCytoplasma));
zStacks.NuclearReceptor.Background = median(zStacks.NuclearReceptor.Intensities(labels.BackgroundCytoplasma));

%% Create Labels  %% Nucleus berechnet als eigener Label 
labels.Cytoplasma.Calculated = zStacks.Cytoplasma.Intensities;
labels.Cytoplasma.Calculated(labels.Cytoplasma.Calculated < zStacks.Cytoplasma.Background * bgMultiplier) = 0;
labels.Cytoplasma.Calculated = labels.Cytoplasma.Calculated .* uint16(labels.Cell.Drawn);
labels.Cytoplasma.Calculated(labels.Cytoplasma.Calculated > 0) = 1;

labels.Cytoplasma.Drawn = uint16(labels.Cell.Drawn);
labels.Cytoplasma.Drawn(uint16(labels.Nucleus.Drawn) == 1) = 0;

zStacks.Nucleus.labels.Nucleus.Drawn = zStacks.Nucleus.Intensities .* uint16(labels.Nucleus.Drawn);
zStacks.Nucleus.labels.Cell.Drawn = zStacks.Nucleus.Intensities .* uint16(labels.Cell.Drawn);
zStacks.Nucleus.labels.Cytoplasma.Calculated = zStacks.Nucleus.Intensities .* uint16(labels.Cytoplasma.Calculated);
zStacks.Nucleus.labels.Cytoplasma.Drawn = zStacks.Nucleus.Intensities .* uint16(labels.Cytoplasma.Drawn);

zStacks.Cytoplasma.labels.Nucleus.Drawn = zStacks.Cytoplasma.Intensities .* uint16(labels.Nucleus.Drawn);
zStacks.Cytoplasma.labels.Cell.Drawn = zStacks.Cytoplasma.Intensities .* uint16(labels.Cell.Drawn);
zStacks.Cytoplasma.labels.Cytoplasma.Calculated = zStacks.Cytoplasma.Intensities .* uint16(labels.Cytoplasma.Calculated);
zStacks.Cytoplasma.labels.Cytoplasma.Drawn = zStacks.Cytoplasma.Intensities .* uint16(labels.Cytoplasma.Drawn);

zStacks.NuclearReceptor.labels.Nucleus.Drawn = zStacks.NuclearReceptor.Intensities .* uint16(labels.Nucleus.Drawn);
zStacks.NuclearReceptor.labels.Cell.Drawn = zStacks.NuclearReceptor.Intensities .* uint16(labels.Cell.Drawn);
zStacks.NuclearReceptor.labels.Cytoplasma.Calculated = zStacks.NuclearReceptor.Intensities .* uint16(labels.Cytoplasma.Calculated);
zStacks.NuclearReceptor.labels.Cytoplasma.Drawn = zStacks.NuclearReceptor.Intensities .* uint16(labels.Cytoplasma.Drawn);

zStacks.NuclearReceptor.IntensitiesWithoutBackground = zStacks.NuclearReceptor.Intensities - zStacks.NuclearReceptor.Background;

%% Volumes
volume.Cell = sum(labels.Cell.Drawn(:)) * zStacksSettings.voxelVolume;
volume.Nucleus = sum(labels.Nucleus.Drawn(:)) * zStacksSettings.voxelVolume;
volume.CytoplasmaLabel = sum(labels.Cytoplasma.Drawn(:)) * zStacksSettings.voxelVolume;
volume.CytoplasmaBG = sum(labels.Cytoplasma.Calculated(:)) * zStacksSettings.voxelVolume;
volume.ratioNucleusCell = volume.Nucleus / volume.Cell;
volume.ratioNucleusCytoplasmaDrawn = volume.Nucleus / volume.CytoplasmaLabel;
volume.ratioNucleusCytoplasmaCalculated = volume.Nucleus / volume.CytoplasmaBG;
volume.unit = "µm³";
volume = struct2table(volume);
volume.name = [filePathNuclearReceptor filePathStainingCytoplasma filePathStainingNucleus];
writetable(volume, fullfile(folder,"zStackAnalysis.xlsx"), 'Sheet','Volumes');
disp("Volumes calculated and saved")
%% Intensitys
intensity.Sum.Raw.Cell = sum(zStacks.NuclearReceptor.labels.Cell.Drawn(:));
intensity.Sum.Raw.CytoplasmaLabel = sum(zStacks.NuclearReceptor.labels.Cytoplasma.Drawn(:));
intensity.Sum.Raw.CytoplasmaBG = sum(zStacks.NuclearReceptor.labels.Cytoplasma.Calculated(:));
intensity.Sum.Raw.Nucleus = sum(zStacks.NuclearReceptor.labels.Nucleus.Drawn(:));

intensity.Sum.BGCorrected.Cell = sum(zStacks.NuclearReceptor.labels.Cell.Drawn(zStacks.NuclearReceptor.labels.Cell.Drawn ~= 0) - zStacks.NuclearReceptor.Background, 'all');
intensity.Sum.BGCorrected.CytoplasmaLabel = sum(zStacks.NuclearReceptor.labels.Cytoplasma.Drawn(zStacks.NuclearReceptor.labels.Cytoplasma.Drawn ~= 0) - zStacks.NuclearReceptor.Background, 'all');
intensity.Sum.BGCorrected.CytoplasmaBG = sum(zStacks.NuclearReceptor.labels.Cytoplasma.Calculated(zStacks.NuclearReceptor.labels.Cytoplasma.Calculated ~= 0) - zStacks.NuclearReceptor.Background,'all');
intensity.Sum.BGCorrected.Nucleus = sum(zStacks.NuclearReceptor.labels.Nucleus.Drawn(zStacks.NuclearReceptor.labels.Nucleus.Drawn ~= 0) - zStacks.NuclearReceptor.Background, 'all');

intensity.Median.Raw.Cell = median(double(zStacks.NuclearReceptor.labels.Cell.Drawn(zStacks.NuclearReceptor.labels.Cell.Drawn ~= 0)));
intensity.Median.Raw.CytoplasmaLabel = median(double(zStacks.NuclearReceptor.labels.Cytoplasma.Drawn(zStacks.NuclearReceptor.labels.Cytoplasma.Drawn ~= 0)));
intensity.Median.Raw.CytoplasmaBG = median(double(zStacks.NuclearReceptor.labels.Cytoplasma.Calculated(zStacks.NuclearReceptor.labels.Cytoplasma.Calculated ~= 0)));
intensity.Median.Raw.Nucleus = median(double(zStacks.NuclearReceptor.labels.Nucleus.Drawn(zStacks.NuclearReceptor.labels.Nucleus.Drawn ~= 0)));

intensity.Median.BGCorrected.Cell = median(double(zStacks.NuclearReceptor.labels.Cell.Drawn(zStacks.NuclearReceptor.labels.Cell.Drawn ~= 0)) - double(zStacks.NuclearReceptor.Background));
intensity.Median.BGCorrected.CytoplasmaLabel = median(double(zStacks.NuclearReceptor.labels.Cytoplasma.Drawn(zStacks.NuclearReceptor.labels.Cytoplasma.Drawn ~= 0)) - double(zStacks.NuclearReceptor.Background));
intensity.Median.BGCorrected.CytoplasmaBG = median(double(zStacks.NuclearReceptor.labels.Cytoplasma.Calculated(zStacks.NuclearReceptor.labels.Cytoplasma.Calculated ~= 0)) - double(zStacks.NuclearReceptor.Background));
intensity.Median.BGCorrected.Nucleus = median(double(zStacks.NuclearReceptor.labels.Nucleus.Drawn(zStacks.NuclearReceptor.labels.Nucleus.Drawn ~= 0)) - double(zStacks.NuclearReceptor.Background));

intensity.Ratio.Sum.Raw.NucleusCell = intensity.Sum.Raw.Nucleus / intensity.Sum.Raw.Cell;
intensity.Ratio.Sum.Raw.NucleusCytoplasmaDrawn = intensity.Sum.Raw.Nucleus / intensity.Sum.Raw.CytoplasmaLabel;
intensity.Ratio.Sum.Raw.NucleusCytoplasmaCalculated = intensity.Sum.Raw.Nucleus / intensity.Sum.Raw.CytoplasmaBG;

intensity.Ratio.Sum.BGCorrected.NucleusCell = intensity.Sum.BGCorrected.Nucleus / intensity.Sum.BGCorrected.Cell;
intensity.Ratio.Sum.BGCorrected.NucleusCytoplasmaDrawn = intensity.Sum.BGCorrected.Nucleus / intensity.Sum.BGCorrected.CytoplasmaLabel;
intensity.Ratio.Sum.BGCorrected.NucleusCytoplasmaCalculated = intensity.Sum.BGCorrected.Nucleus / intensity.Sum.BGCorrected.CytoplasmaBG;

intensity.Ratio.Median.Raw.NucleusCell = intensity.Median.Raw.Nucleus / intensity.Median.Raw.Cell;
intensity.Ratio.Median.Raw.NucleusCytoplasmaDrawn = intensity.Median.Raw.Nucleus / intensity.Median.Raw.CytoplasmaLabel;
intensity.Ratio.Median.Raw.NucleusCytoplasmaCalculated = intensity.Median.Raw.Nucleus / intensity.Median.Raw.CytoplasmaBG;

intensity.Ratio.Median.BGCorrected.NucleusCell = intensity.Median.BGCorrected.Nucleus / intensity.Median.BGCorrected.Cell;
intensity.Ratio.Median.BGCorrected.NucleusCytoplasmaDrawn = intensity.Median.BGCorrected.Nucleus / intensity.Median.BGCorrected.CytoplasmaLabel;
intensity.Ratio.Median.BGCorrected.NucleusCytoplasmaCalculated = intensity.Median.BGCorrected.Nucleus / intensity.Median.BGCorrected.CytoplasmaBG;

flatStruct = flattenStruct(intensity);

intensityTable = struct2table(flatStruct);
intensityTable.name = [filePathNuclearReceptor filePathStainingCytoplasma filePathStainingNucleus];

writetable(intensityTable, fullfile(folder,"zStackAnalysis.xlsx"), 'Sheet','Intensity');
disp("Intensities calculated and saved")

%% Volumen korrigiert %%
% Calculate intensities divided by respective volume and save them in the struct
intensityDividedByVolume.Sum.Raw.Cell = intensity.Sum.Raw.Cell / volume.Cell;
intensityDividedByVolume.Sum.Raw.CytoplasmaLabel = intensity.Sum.Raw.CytoplasmaLabel / volume.CytoplasmaLabel;
intensityDividedByVolume.Sum.Raw.CytoplasmaBG = intensity.Sum.Raw.CytoplasmaBG / volume.CytoplasmaBG;
intensityDividedByVolume.Sum.Raw.Nucleus = intensity.Sum.Raw.Nucleus / volume.Nucleus;

intensityDividedByVolume.Sum.BGCorrected.Cell = intensity.Sum.BGCorrected.Cell / volume.Cell;
intensityDividedByVolume.Sum.BGCorrected.CytoplasmaLabel = intensity.Sum.BGCorrected.CytoplasmaLabel / volume.CytoplasmaLabel;
intensityDividedByVolume.Sum.BGCorrected.CytoplasmaBG = intensity.Sum.BGCorrected.CytoplasmaBG / volume.CytoplasmaBG;
intensityDividedByVolume.Sum.BGCorrected.Nucleus = intensity.Sum.BGCorrected.Nucleus / volume.Nucleus;

intensityDividedByVolume.Median.Raw.Cell = intensity.Median.Raw.Cell / volume.Cell;
intensityDividedByVolume.Median.Raw.CytoplasmaLabel = intensity.Median.Raw.CytoplasmaLabel / volume.CytoplasmaLabel;
intensityDividedByVolume.Median.Raw.CytoplasmaBG = intensity.Median.Raw.CytoplasmaBG / volume.CytoplasmaBG;
intensityDividedByVolume.Median.Raw.Nucleus = intensity.Median.Raw.Nucleus / volume.Nucleus;

intensityDividedByVolume.Median.BGCorrected.Cell = intensity.Median.BGCorrected.Cell / volume.Cell;
intensityDividedByVolume.Median.BGCorrected.CytoplasmaLabel = intensity.Median.BGCorrected.CytoplasmaLabel / volume.CytoplasmaLabel;
intensityDividedByVolume.Median.BGCorrected.CytoplasmaBG = intensity.Median.BGCorrected.CytoplasmaBG / volume.CytoplasmaBG;
intensityDividedByVolume.Median.BGCorrected.Nucleus = intensity.Median.BGCorrected.Nucleus / volume.Nucleus;

% Calculate the ratio data as real ratios
intensityDividedByVolume.Ratio.Sum.Raw.NucleusCell = intensityDividedByVolume.Sum.Raw.Nucleus / intensityDividedByVolume.Sum.Raw.Cell;
intensityDividedByVolume.Ratio.Sum.Raw.NucleusCytoplasmaDrawn = intensityDividedByVolume.Sum.Raw.Nucleus / intensityDividedByVolume.Sum.Raw.CytoplasmaLabel;
intensityDividedByVolume.Ratio.Sum.Raw.NucleusCytoplasmaCalculated = intensityDividedByVolume.Sum.Raw.Nucleus / intensityDividedByVolume.Sum.Raw.CytoplasmaBG;

intensityDividedByVolume.Ratio.Sum.BGCorrected.NucleusCell = intensityDividedByVolume.Sum.BGCorrected.Nucleus / intensityDividedByVolume.Sum.BGCorrected.Cell;
intensityDividedByVolume.Ratio.Sum.BGCorrected.NucleusCytoplasmaDrawn = intensityDividedByVolume.Sum.BGCorrected.Nucleus / intensityDividedByVolume.Sum.BGCorrected.CytoplasmaLabel;
intensityDividedByVolume.Ratio.Sum.BGCorrected.NucleusCytoplasmaCalculated = intensityDividedByVolume.Sum.BGCorrected.Nucleus / intensityDividedByVolume.Sum.BGCorrected.CytoplasmaBG;

intensityDividedByVolume.Ratio.Median.Raw.NucleusCell = intensityDividedByVolume.Median.Raw.Nucleus / intensityDividedByVolume.Median.Raw.Cell;
intensityDividedByVolume.Ratio.Median.Raw.NucleusCytoplasmaDrawn = intensityDividedByVolume.Median.Raw.Nucleus / intensityDividedByVolume.Median.Raw.CytoplasmaLabel;
intensityDividedByVolume.Ratio.Median.Raw.NucleusCytoplasmaCalculated = intensityDividedByVolume.Median.Raw.Nucleus / intensityDividedByVolume.Median.Raw.CytoplasmaBG;

intensityDividedByVolume.Ratio.Median.BGCorrected.NucleusCell = intensityDividedByVolume.Median.BGCorrected.Nucleus / intensityDividedByVolume.Median.BGCorrected.Cell;
intensityDividedByVolume.Ratio.Median.BGCorrected.NucleusCytoplasmaDrawn = intensityDividedByVolume.Median.BGCorrected.Nucleus / intensityDividedByVolume.Median.BGCorrected.CytoplasmaLabel;
intensityDividedByVolume.Ratio.Median.BGCorrected.NucleusCytoplasmaCalculated = intensityDividedByVolume.Median.BGCorrected.Nucleus / intensityDividedByVolume.Median.BGCorrected.CytoplasmaBG;

flatStruct = flattenStruct(intensityDividedByVolume);

intensityDividedByVolumeTable = struct2table(flatStruct);
intensityDividedByVolumeTable.name = [filePathNuclearReceptor filePathStainingCytoplasma filePathStainingNucleus];

writetable(intensityDividedByVolumeTable, fullfile(folder,"zStackAnalysis.xlsx"), 'Sheet','IntensityDividedByVolume');
disp("Intensities divided by volume calculated and saved")


% histoCell = figure;
% title("Cell")
% histogram(zStacks.NuclearReceptor.labels.Cell.Drawn(zStacks.NuclearReceptor.labels.Cell.Drawn ~= 0) - zStacks.NuclearReceptor.Background)
% histoCytoplasmaCalc = figure;
% title("Cytoplasma Calculated")
% histogram(zStacks.NuclearReceptor.labels.Cytoplasma.Calculated(zStacks.NuclearReceptor.labels.Cytoplasma.Calculated ~= 0) - zStacks.NuclearReceptor.Background);
% histoCytoplasmaDrawn = figure;
% title("Cytoplasma Drawn")
% histogram(zStacks.NuclearReceptor.labels.Cytoplasma.Drawn(zStacks.NuclearReceptor.labels.Cytoplasma.Drawn ~= 0) - zStacks.NuclearReceptor.Background);
% histoNucleus = figure;
% title("Nucleus")
% histogram(zStacks.NuclearReceptor.labels.Nucleus.Drawn(zStacks.NuclearReceptor.labels.Nucleus.Drawn ~= 0) - zStacks.NuclearReceptor.Background);
% disp("Histograms created")

save(fullfile(folder,"usedData.mat"));
disp("Data was saved");
end