function [zStacks, volumes, intensity] =  clusterAnalysisFunction(zStacksSettings, minVoxelSize, threshold_cor, plotSettings, folder, fileNameNuclearReceptor)
  

    % Check for valid folder and file paths
if ~(isstring(folder)  || ~ischar(folder)) || ...
   ~(isstring(fileNameNuclearReceptor) || ~ischar(fileNameNuclearReceptor))
    error('Invalid input: folder and file names must be strings or character arrays.');
end

disp("Reading in data");
filePathNuclearReceptor = fullfile(folder, fileNameNuclearReceptor);


zStacks.NuclearReceptor.Raw.Intensities = tiffreadVolume(filePathNuclearReceptor);

labels.BackgroundCluster = load(fullfile(folder,'labelCytoplasmaBG.mat')).labels;
disp("All data succesfully loaded")

zStacks.NuclearReceptor.Background.Intensities = median(zStacks.NuclearReceptor.Raw.Intensities(labels.BackgroundCluster));
zStacks.NuclearReceptor.IntensitiesWithoutBackground.Intensities = zStacks.NuclearReceptor.Raw.Intensities - zStacks.NuclearReceptor.Background.Intensities;


zStacks.NuclearReceptor.IntensitiesWithoutBackground.threshold = graythresh(zStacks.NuclearReceptor.IntensitiesWithoutBackground.Intensities);

% Convert grayscale image to binary using the threshold
zStacks.NuclearReceptor.IntensitiesWithoutBackground.binaryStack = imbinarize(zStacks.NuclearReceptor.IntensitiesWithoutBackground.Intensities, zStacks.NuclearReceptor.IntensitiesWithoutBackground.threshold + threshold_cor);

% Perform morphological reconstruction to merge nearby regions
se = strel('sphere', 1); % Adjust the size of the structuring element as needed
zStacks.NuclearReceptor.IntensitiesWithoutBackground.binaryStackReconstructed = imdilate(zStacks.NuclearReceptor.IntensitiesWithoutBackground.binaryStack, se);

% Perform 3D connected component analysis on the reconstructed image
zStacks.NuclearReceptor.IntensitiesWithoutBackground.ccReconstructed = bwconncomp(zStacks.NuclearReceptor.IntensitiesWithoutBackground.binaryStackReconstructed, 26); % 26-connectivity for 3D


% Filter out smaller objects
zStacks.NuclearReceptor.IntensitiesWithoutBackground.ccFiltered = zStacks.NuclearReceptor.IntensitiesWithoutBackground.ccReconstructed;
zStacks.NuclearReceptor.IntensitiesWithoutBackground.ccFiltered.PixelIdxList = zStacks.NuclearReceptor.IntensitiesWithoutBackground.ccReconstructed.PixelIdxList(cellfun(@numel, zStacks.NuclearReceptor.IntensitiesWithoutBackground.ccReconstructed.PixelIdxList) >= minVoxelSize);
zStacks.NuclearReceptor.IntensitiesWithoutBackground.ccFiltered.NumObjects = width(zStacks.NuclearReceptor.IntensitiesWithoutBackground.ccFiltered.PixelIdxList);
zStacks.NuclearReceptor.IntensitiesWithoutBackground.props = regionprops3(zStacks.NuclearReceptor.IntensitiesWithoutBackground.ccFiltered, 'all');


volumes = zStacks.NuclearReceptor.IntensitiesWithoutBackground.props.Volume;
intensity.Sum.Raw = cellfun(@(x) sum(zStacks.NuclearReceptor.Raw.Intensities(x),'all'), zStacks.NuclearReceptor.IntensitiesWithoutBackground.props.VoxelIdxList, 'UniformOutput', false);
intensity.Median.Raw = cellfun(@(x) median(nonzeros(double(zStacks.NuclearReceptor.Raw.Intensities(x)))), zStacks.NuclearReceptor.IntensitiesWithoutBackground.props.VoxelIdxList, 'UniformOutput', false);
intensity.Sum.dividedbyVolume.Raw = cell2mat(intensity.Sum.Raw) ./ volumes * zStacksSettings.voxelVolume;
intensity.Median.dividedbyVolume.Raw = cell2mat(intensity.Median.Raw) ./ volumes * zStacksSettings.voxelVolume;

intensity.Sum.BackgroundCorrected = cellfun(@(x) sum(zStacks.NuclearReceptor.IntensitiesWithoutBackground.Intensities(x),'all'), zStacks.NuclearReceptor.IntensitiesWithoutBackground.props.VoxelIdxList, 'UniformOutput', false);
intensity.Median.BackgroundCorrected = cellfun(@(x) median(nonzeros(double(zStacks.NuclearReceptor.IntensitiesWithoutBackground.Intensities(x)))), zStacks.NuclearReceptor.IntensitiesWithoutBackground.props.VoxelIdxList, 'UniformOutput', false);
intensity.Sum.dividedbyVolume.BackgroundCorrected = cell2mat(intensity.Sum.BackgroundCorrected) ./ volumes * zStacksSettings.voxelVolume;
intensity.Median.dividedbyVolume.BackgroundCorrected = cell2mat(intensity.Median.BackgroundCorrected) ./ volumes * zStacksSettings.voxelVolume;

flatStruct = flattenStruct(intensity);

clusterTable = struct2table(flatStruct);
clusterTable.Voxel = volumes;
clusterTable.Volume = volumes * zStacksSettings.voxelVolume;
clusterTable.VolumeUnit = repelem("µm³", height(clusterTable))';
clusterTable.Name = repmat(filePathNuclearReceptor, height(clusterTable),1);
clusterTable.Cluster = (1:height(clusterTable))';

writetable(clusterTable, fullfile(folder,"ClusterAnalysis.xlsx"), 'Sheet','Clusters');
disp("Clusters detected and saved")


% Create a 3D figure using volshow
hVolume = VolumePlot(zStacksSettings, zStacks.NuclearReceptor.IntensitiesWithoutBackground.Intensities, plotSettings, 'grad'); % You can adjust the renderer based on your preference


% Create a binary overlay highlighting the traced cluster boundaries
overlayData = zeros(size(zStacks.NuclearReceptor.IntensitiesWithoutBackground.Intensities));
for k = 1:length(zStacks.NuclearReceptor.IntensitiesWithoutBackground.ccFiltered.PixelIdxList)
    overlayData(zStacks.NuclearReceptor.IntensitiesWithoutBackground.ccFiltered.PixelIdxList{k}) = 1;
end

% Overlay the traced cluster boundaries on the 3D volume using 'OverlayData'
hVolume.OverlayAlphamap = 0.9;
hVolume.OverlayRenderingStyle = "LabelOverlay";
hVolume.OverlayData = overlayData;

save(fullfile(folder,"usedData.mat"));
